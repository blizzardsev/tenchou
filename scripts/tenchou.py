from datetime import datetime
import os
import sys
import time
import requests
sys.path.insert(0, 'packages')
import discord
from discord.errors import *

# Setup
client = discord.Client()

# Constants

# Image processing
_SESSION_LOG_FILE_NAME = f'log/{int(time.time())}.txt'
_ACCEPTED_FILETYPES = ['image/jpeg','image/png','image/svg','image/gif']
_TARGET_CHANNEL_ID = int(sys.argv[1])
_DESTINATION_CHANNEL_ID = int(sys.argv[2])
_IMAGE_DIR = os.path.join(os.getcwd(), 'img')

def write_to_log(content, print_to_console=False):
    """
    Writes the given content, prefixed with a localised timestamp, to the configured log file.
    Log files are stored under log/ in the current working directory.
    Each log corresponds to a single session.

    IN:
        - content - The string message to write to the log
        - print_to_console - True if the content should also be printed to the command prompt
    """
    global _SESSION_LOG_FILE_NAME
    content_with_stamp = f'{datetime.now().strftime("%c")}: {content}'
    with open(os.path.join(os.getcwd(), _SESSION_LOG_FILE_NAME), 'a+') as log_file:
        log_file.write(content_with_stamp)

    if (print_to_console):
        print(content_with_stamp)

@client.event
async def on_ready():
    """
    Called when the bot has successfully connected to Discord.
    Outputs some basic connection info, and performs some other setup steps.
    """
    global _TARGET_CHANNEL_ID
    global _DESTINATION_CHANNEL_ID
    launch_info = f'Connected, listening to channel id {_TARGET_CHANNEL_ID}, posting to channel id {_DESTINATION_CHANNEL_ID}'
    write_to_log(launch_info, True)

@client.event
async def on_message(message):
    """
    Called when the bot detects a message has been sent to the target channel.
    Triggers the process to parse message for image attachments and upload them as embeds to destination channel.
    
    IN:
        - message - The message that has just been sent
    """
    global _TARGET_CHANNEL_ID
    global _ACCEPTED_FILETYPES
    global _IMAGE_DIR

    if (message.channel.id == _TARGET_CHANNEL_ID
            and not message.author.bot 
            and not message.content.startswith('nopost')
            and len(message.attachments) > 0):

            # React to the message
            try:
                await message.add_reaction('💗')

            except Forbidden:
                write_to_log('Failed to add reaction; insufficient permissions to add reactions', True)
            
            except NotFound:
                write_to_log('Failed to add reaction; failed to find the emoji', True)

            except HTTPException:
                write_to_log('Failed to add reaction; emoji is unknown', True)

            except (ValueError, TypeError):
                write_to_log('Failed to add reaction; invalid emoji specified', True)

            # Possible for a user to upload multiple images per message, so we check all
            for attachment in message.attachments:
                if attachment.content_type in _ACCEPTED_FILETYPES:
                    try:
                        # Attachment is an image; download it
                        response = requests.get(attachment.url)
                        if response.status_code == 200:
                            with open(f'{_IMAGE_DIR}/{int(time.time())}.png', 'wb+') as image:
                                image.write(response.content)
                        del response

                    except:
                        write_to_log(f'Failed to process attachment for message id {message.id}')
                    
            for image in os.listdir(_IMAGE_DIR):
                # Repost each image, then remove it from storage
                await create_send_embed(
                    image=image,
                    poster_name=message.author.name
                )
                try:
                    os.remove(f'{_IMAGE_DIR}/{image}')

                except FileNotFoundError:
                    write_to_log(f'Image deletion failed; {image} was not found', True)

async def create_send_embed(image, poster_name=None):
    """
    Creates a Discord embed with the image and a basic footer,
    then posts it to Discord to the destination channel.

    IN:
        - image - The filename of the image, as found under the /img folder
        - poster_name - The name of the author who posted the last image in the target channel
    """
    global _DESTINATION_CHANNEL_ID
    global _IMAGE_DIR

    embed = discord.Embed(
        description='',
        color=16224167)

    if not poster_name:
        poster_name = 'Anonymous'

    file = discord.File(f'{_IMAGE_DIR}/{image}')
    embed.set_image(url=f'attachment://{image}')
    embed.set_footer(text=f'Posted at {datetime.now().strftime("%c")} by {poster_name}')

    try:
        channel = await client.fetch_channel(_DESTINATION_CHANNEL_ID)
        await channel.send(file=file, embed=embed)

    except Forbidden:
        write_to_log('Failed to post embed to destination channel; insufficient permissions to send messages', True)

    except:
        write_to_log('Failed to post embed to destination channel; an unknown exception occurred', True)

try:
    # Entry point - attempt to connect to Discord
    client.run(sys.argv[3])

except:
    write_to_log('Failed to initiate client; check bot token', True)
