@echo off
Rem This script will automatically deduce and install the dependencies for tenchou, before starting it.
Rem Params:
Rem %1: Target channel id
Rem %2: Destination channel id
Rem %3: Discord bot token

echo Fetching requirements...
rem Builds requirements.txt, whilst ignoring any packages already present
cd scripts
pigar -i packages
cd ..

echo Downloading dependencies...
rem Installs (non-standard with Python) dependencies to the packages directory
pip install -r scripts/requirements.txt -t scripts/packages --upgrade

echo Deploying..
python scripts/tenchou.py %1 %2 %3
echo Deployment complete.

echo Script completed. Last updated: %time%
pause
