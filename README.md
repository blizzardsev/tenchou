# About tenchou

Tenchou is a simple Python-based Discord bot designed to listen to image posts in a specified target channel, and download/repost those same images in a destination channel.

## CI/deployment script configuration

### Dependencies

* **pigar**: Automatic Python requirements.txt @ https://github.com/damnever/pigar

## Python configuration

### Runtime

* Python 3.8

### Dependencies

* requests: HTTP for humans @: https://docs.python-requests.org/en/latest/
* discord.py: Python Discord API @: https://discordpy.readthedocs.io/en/stable/

## Deployment

Use script *deploy.bat* *target-channel-id* *destination-channel-id* *bot-token*; E.G `deploy.bat 123 456 ABC123`. Execute within Tenchou directory. This is required *at least once* to allow Tenchou to install required dependencies, etc.

After this, Tenchou may be run using the command prompt as a Python file. Remember to include the parameters listed above.

As Tenchou is a Discord bot, it should be hosted somewhere uptime can be guaranteed (I.E preferably not on a desktop PC).

## Directory structure

* img: Location for temporarily downloaded images when reposting
* log: Location for log files. A new log file is created per instantiation/deployment

## Contact

Please raise any suggestions, etc. using the Issue Tracker.

Email: *blizzardsev.dev@gmail.com*
